package com.xdw.devopsdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DevopsController {
    @GetMapping("/")
    public String index(){
        Logger logger = LoggerFactory.getLogger(DevopsController.class);
        logger.info("haahh");
        return "Springboot devops demo";
    }
}
